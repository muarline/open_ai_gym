FROM tensorflow/tensorflow:latest-gpu-py3

RUN apt-get install -y freeglut3-dev xvfb

RUN echo "\n\n" | jupyter notebook password
RUN pip3 install gym

CMD xvfb-run -s "-screen 0 1400x900x24" jupyter notebook --allow-root
