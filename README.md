# Container for providing easy gym render in jupyter notebooks

[Docker hub](https://hub.docker.com/r/muarline/open_ai_gym/)

**Base container:**
- tensorflow/tensorflow:latest-gpu-py3

**Requirements:**
- nvidia-docker
- GPU

**How to run?**
- Docker-compose:
`docker-compose up -d`

- Docker:
`docker run --runtime=nvidia -ti -p 8888:8888 -v /path/to/notebooks:/notebooks muarilne/open_ai_gym:latest`

Also sets blank password ('') to jupyter notebook

**Examples:**

1 CardPole-v0

```
def render_env(env):
    plt.figure(1)
    plt.clf()
    plt.imshow(env.render(mode='rgb_array'))
    plt.axis('off')

    display.display(plt.gcf())
    display.clear_output(wait=True)

import gym
env = gym.make('CartPole-v0')
env.reset()
for step in range(100):
    render_env(env)
    env.step(env.action_space.sample())
```

